package croclabs.http;

import java.io.IOException;
import java.net.Authenticator;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

public final class Http {
    private static final Logger LOG = Logger.getLogger(Http.class.getName());

    HttpRequest.Builder requestBuilder;
    HttpClient.Builder clientBuilder;

    private Http() {
        super();
    }

    public static Http create(String uri) {
        try {
            return create(new URI(uri));
        } catch (URISyntaxException e) {
            LOG.severe("Could not create URI.");
            e.printStackTrace();
            return new Http();
        }
    }

    public static Http create(URL url) {
        try {
            return create(url.toURI());
        } catch (URISyntaxException e) {
            LOG.severe("Could not create URI.");
            e.printStackTrace();
            return new Http();
        }
    }

    public static Http create(URI uri) {
        Http http = new Http();
        http.requestBuilder = HttpRequest.newBuilder().uri(uri).GET();
        http.clientBuilder = HttpClient.newBuilder();
        return http;
    }

    public Http request(IHttp http) {
        http.request(requestBuilder);
        return this;
    }

    public Http client(IClient client) {
        client.client(clientBuilder);
        return this;
    }

    public <K> Then<HttpResponse<K>> send(BodyHandler<K> handler) {
        HttpClient client = clientBuilder.build();
        HttpRequest request = requestBuilder.build();

        return new Then<>(client.sendAsync(request, handler));
    }

    public final static class Then<T> {
        CompletableFuture<T> future;

        Then(CompletableFuture<T> future) {
            super();
            this.future = future;
        }

        public void then(Consumer<T> action) {
            future.thenAccept(action);
        }

        public <U> Then<U> thenReturn(Function<T, U> action) {
            return new Then<>(future.thenApply(action));
        }

        public T thenReturn() {
            try {
                return this.future.get();
            } catch (InterruptedException | ExecutionException e) {
                LOG.severe("Getting result failed.");
                e.printStackTrace();
                return null;
            }
        }
    }

    @FunctionalInterface
    public interface IHttp {
        void request(HttpRequest.Builder request);
    }

    @FunctionalInterface
    public interface IClient {
        void client(HttpClient.Builder client);
    }
}
