package croclabs;

import croclabs.http.Http;

import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.logging.Logger;

public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Hello world!");

        Http.create("https://www.google.com")
                .send(BodyHandlers.ofString())
                .then(res -> {
                    try {
                        Thread.sleep(3000);
                        LOG.info("1: " + res.body());
                    } catch (Exception ignored) {

                    }
                });

        Http.create("https://www.google.com")
                .send(BodyHandlers.ofString())
                .thenReturn(HttpResponse::body)
                .then(body -> {
                    try {
                        Thread.sleep(1000);
                        LOG.info("2: " + body);
                    } catch (Exception ignored) {

                    }
                });

        Thread.sleep(16000);
    }
}